
import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
*
* @author manel
*/
public class Main {
	 /**
	 * @param args the command line arguments
	 */
	 public static void main(String[] args) {
		 // TODO code application logic here
		 System.out.println("Server listening port 8189...");
		 ClientsController clientController;
		 clientController = new ClientsController();
		 int contador = 0;
		 try {
			 ServerSocket server = new ServerSocket(8189);
			 while (true) {
				 Socket socket = server.accept();
				 System.out.println("Client numero " + contador +" Conectat");
				 Runnable r = new ServerManagement(socket, contador, clientController);
				 Thread t = new Thread(r);
				 t.start();
				 contador++;
			 }
		 } catch (IOException ioe) {
			 ioe.printStackTrace(); 
		 }
	}
}