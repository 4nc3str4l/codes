
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class ServerManagement implements Runnable {
	 
	private Socket socket;
	private int contador;
	Scanner in;
	private ClientsController clientsController;
	public ServerManagement(Socket i, int c, ClientsController clientsController) {
		 socket = i;
		 contador = c;
		 this.clientsController = clientsController;
	 }
	 public void run() {
		 try {
			 try {
				 InputStream entrada = socket.getInputStream();
				 OutputStream salida = socket.getOutputStream();
				 in = new Scanner(entrada);
				 PrintWriter out = new PrintWriter(salida,true);
				 out.println("<font color='blue'><b>Server:</b> Connectat. Escriu BYE o tanca la finestra per sortir o escriu /whois per saber qui hi ha online</font>");
				 clientsController.addClient(socket, contador);//afegim un nou client
				 clientsController.sendMessageToID("/server: 0x03"+contador, contador);
				 boolean fin = true;
				 while (fin || in.hasNextLine()){
						 String linia = in.nextLine();
						 if(linia.trim().equals("BYE")){//si el client es vol desconectar
							 fin = false;
							 this.clientsController.clientDisconect(contador);
							 System.out.println("Client "+contador+" Desconectat");
						 }if(linia.trim().equals("/whois")){//Si el client vol saber qui hi ha conectat
							 this.clientsController.whois(contador);
						 }
						 else{//si el client vol enviar un misatge a tots
							 clientsController.broadcastMessage(linia, contador);
						 }
				 }
					 
			 } finally {//si el client es desconecta tanquem els elements oberts.
				 socket.close();
				 in.close();
				 this.clientsController.clientDisconect(contador);
				 
			 }
		 }catch (Exception ex) {//mostrem per consola que un client s'ha desconectat.
			 System.out.println("Client "+contador+" Desconectat");
		 }
	 }

}