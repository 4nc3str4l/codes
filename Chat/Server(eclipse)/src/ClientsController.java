import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Aquesta es la classe mes important ja que s'encarrega de enviar misatges als clients
 * respondre a comandes, controlar quins clients estan conectats, informar...
 * @author cristian
 *
 */
public class ClientsController{
	public ArrayList<Client> clients; //LLista que guarda els clients conectats
	
	public ClientsController(){
		this.clients = new ArrayList<Client>();
	}
	
	/**
	 * Aquest metode envia un missatge a un client determinat
	 * @param message
	 * @param id
	 */
	public void sendMessageToID(String message, int id){
		PrintWriter outToClient;
		try {
			outToClient = new PrintWriter(new OutputStreamWriter(this.clients.get(this.findIndexClientByID(id)).getSocket().getOutputStream()));
	        outToClient.println(message);
	        outToClient.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Aquest metode envia un missatge a tots els clients conectats al servidor.
	 * @param message
	 * @param idSender
	 */
	public void broadcastMessage(String message, int idSender){
		PrintWriter outToClient = null;
		for(int i = 0;i< this.clients.size();i++){
			try {
				outToClient = new PrintWriter(new OutputStreamWriter(this.clients.get(i).getSocket().getOutputStream()));
                if(idSender != -1) outToClient.println("<b>Client "+idSender+":</b> " + message);
                else outToClient.println(message);
                outToClient.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Aquest metode afegeix un client a la llista i informa a tots els usuaris de que el client s'ha conectat.
	 * @param socket
	 * @param id
	 */
	public void addClient(Socket socket, int id){
		this.clients.add(new Client(socket,id));
		this.updateNumOnlineUsers();
		this.broadcastMessage("/server: 0x01"+"client "+id, -1);
	}
	
	/**
	 * Aquest metode borra un client de la llista i a part infroma a tots els clients de que aquest ha estat desconectat.
	 * @param id
	 */
	public void clientDisconect(int id){
		this.clients.remove(this.findIndexClientByID(id));
		this.broadcastMessage("/server: 0x02"+"client "+id, -1);
		this.updateNumOnlineUsers();
	}
	
	/**
	 * Troba un client a la llista usant la seva id.
	 * @param id
	 * @return
	 */
	public int findIndexClientByID(int id){
		boolean found = false;
		int index = 0;
		while(!found){
			if(this.clients.get(index).getId() == id){
				found = true;
			}else{
				index ++;
			}
		}
		return index;
	}
	/**
	 * Aquest metode serveix per comptar cuants clients estan conectats
	 * @return
	 */
	public int getNunClientsOnline(){
		return this.clients.size();
	}

	/**
	 * Aquest metode actualitza el numero de clients que hi ha a la GUI del chat en temps real.
	 */
	public void updateNumOnlineUsers() {
		this.broadcastMessage("/server: 0x00"+this.clients.size(), -1);
	}

	/**
	 * Aquest metode informa al client que hagi invocat la comanda /whois de quins clients hi han online.
	 * @param id
	 */
	public void whois(int id) {
		String clients = "<br>Gent Online:<br>-----------------------------";
		for(int i=0;i<this.clients.size();i++){
			clients+="<br><b>-Client "+this.clients.get(i).getId()+"</b>";
		}
		clients+="<br>-----------------------------";
		this.sendMessageToID("/server: 0x04"+clients, id);
		
	}


}
