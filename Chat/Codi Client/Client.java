
import java.awt.Color;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * Aquesta classe es l'encarregada de mostrar misatges al client, captar els misatges 
 * que el client escriu en el teclat y realitzar els canvis
 * que el client desitgi.
 */

/**
 *
 * @author cristian
 */
public class Client extends javax.swing.JFrame {

    ServerListener sl;//thread que escolta al servidor
    ServerSender ss;//clase que enviara els misatges del client al servidor
    Socket socket;
    private boolean conected;//Ens diu si estem conectats a un servidor
    private boolean makeNoise;//ens diu si les notificacions sonores estan activades o no.
    private ArrayList<String> messages;//guarda els misatges que anem rebent
    String color;//Color que desitjem codificat en rgb optimitzar per html.
    Color actualColor;//color actual com a color de java.
    /**
     * Creates new form Client
     */
    public Client() {
        initComponents();
        this.makeNoise = true;
        this.setResizable(false);//no podem cambiar el tamany de la GUI
        this.messagesField.setEditable(false);
        this.messagesField.setContentType("text/html");//escriurem en html (aixo es de cara al colors i la negreta)
        this.messages = new ArrayList<String>();
        this.setTitle("Xarxes Practica 1");
    }
    public void connect(String host,int port){
        if(!this.conected){//si no estem conectats ens conectem
            try { 
                socket = new Socket(host, port);
                this.conected = true;
            } catch (IOException ex) {//si el servidor esta "down"
                this.showMessage("<font color='red'><b>Chat: </b>No puc conectar-me amb el servidor!</font>");
            }
            Runnable r = new ServerListener(socket,this);//posem a escoltar el thread que rebra els misatges del server.
            ss = new ServerSender(socket);
            Thread t = new Thread(r);
            t.start();
        }else{//si ja estem conectats...
            this.showMessage("<font color='red'><b>Chat: </b>Ya estas conectat!</font>");
        }
    }

    /**   
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        host = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        port = new javax.swing.JTextField();
        connectButton = new javax.swing.JButton();
        chatfield = new javax.swing.JScrollPane();
        messagesField = new javax.swing.JEditorPane();
        sendButton = new javax.swing.JButton();
        messageBox = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        numUsers = new javax.swing.JLabel();
        soundButton = new javax.swing.JButton();
        indentificador = new javax.swing.JLabel();
        colorbutton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        about = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setText("IP:");

        host.setText("localhost");

        jLabel2.setText("Port:");

        port.setText("8189");

        connectButton.setText("Connectar");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });

        chatfield.setViewportView(messagesField);

        sendButton.setText("Enviar");
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });

        messageBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                messageBoxKeyPressed(evt);
            }
        });

        jLabel3.setText("Conected Users:");

        numUsers.setText("0");

        soundButton.setText("sound: on");
        soundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                soundButtonActionPerformed(evt);
            }
        });

        indentificador.setForeground(new java.awt.Color(0, 0, 255));
        indentificador.setText("Estat: Desconectat");

        colorbutton.setText("Color text");
        colorbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorbuttonActionPerformed(evt);
            }
        });

        jMenu1.setText("Menu");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        about.setText("About");
        about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutActionPerformed(evt);
            }
        });
        jMenu1.add(about);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chatfield, javax.swing.GroupLayout.PREFERRED_SIZE, 792, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(messageBox, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sendButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(indentificador)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(numUsers)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(colorbutton)
                                .addGap(18, 18, 18)
                                .addComponent(soundButton)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(host, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(port, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(connectButton)))))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(indentificador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(connectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(port, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(host, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(numUsers)
                    .addComponent(soundButton)
                    .addComponent(colorbutton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chatfield, javax.swing.GroupLayout.PREFERRED_SIZE, 409, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(messageBox, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sendButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Metode que ens serveix per mostrar un misatge per consola o be fer el que el servidor vulgui
     * */
    public void showMessage(String message){
        if(message.length() > 13 && message.substring(0,13).equals("/server: 0x00")){//Si el servidor vol actualizar el nombre de usuaris
            this.numUsers.setText(message.substring(13,message.length()));
        }else if(message.length() > 13 && message.substring(0,13).equals("/server: 0x01")){//nou usuari conectat
            this.showMessage("<font color='blue'><b>Server:</b> El "+message.substring(13,message.length())+" s'ha conectat</font>");
        }else if(message.length() > 13 && message.substring(0,13).equals("/server: 0x02")){//usuari desconectat
            this.showMessage("<font color='blue'><b>Server:</b> El "+message.substring(13,message.length())+" s'ha desconectat</font>");
        }else if(message.length() > 13 && message.substring(0,13).equals("/server: 0x03")){//el server t'informa de qui ets
            this.indentificador.setText("Estat: Client "+message.substring(13,message.length()));
        }else if(message.length() > 13 && message.substring(0,13).equals("/server: 0x04")){//has fet un whois
            this.showMessage("<font color='blue'><b>Server:</b>"+message.substring(13,message.length())+"</font>");
        }
        else{
            if(this.makeNoise){//si les alertes sonores estan activades al rebre un missatge la fem.
                Sound.notification.play();
            }
            
            //mostrem el misatge.
            this.messages.add(message+"<br>");  
            String chat = "";
            for(int i = 0;i<this.messages.size();i++){
                chat += this.messages.get(i);
            }
            this.messagesField.setText(chat);
            //Fem scroll down per veure sempre l'ultim missatge.
            this.messagesField.setCaretPosition(this.messagesField.getDocument().getLength());
        }
    }
    /**
     * Aquest metode es l'encarregat d'enviar els missatges al pitjar el boto o al premer enter.
     * @param message 
     */
    private void sendMessage(String message){
        if(this.conected){
            if(message.length() > 0){
                if(message.equals("/whois")){//si fem whois ho tractem de un mode especial
                    this.ss.SendMessage(message);
                }
                if(message.equals("BYE")){//al fer bye tanquem.
                    this.ss.SendMessage(message);
                    this.dispose();
                }else{//Si el misatge es normal l'enviem.
                    this.ss.SendMessage("<font color="+this.color+">"+message+"</font>");
                }
                this.messageBox.setText("");//borrem el camp del missatge 
            }
        }else{//si no estem conectats mostrem un misatge.
            this.showMessage("<font color='red'><b>Chat:</b> No estas conectat a cap servidor!</font>");
        }
    }
    /**
     * Al premer el boto conectar ens conectem.
     * @param evt 
     */
    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        // TODO add your handling code here:
        this.connect(this.host.getText(), Integer.parseInt(this.port.getText()));
    }//GEN-LAST:event_connectButtonActionPerformed
    
    /**
     * Al premer el boto enviar enviem el que hi ha en el text.
     * @param evt 
     */
    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        // TODO add your handling code here:
        this.sendMessage(this.messageBox.getText());
    }//GEN-LAST:event_sendButtonActionPerformed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:

    }//GEN-LAST:event_formKeyPressed
    
    /**
     * Si apretem enter enviem un missatge.
     * @param evt 
     */
    private void messageBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_messageBoxKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode() == 10){
            this.sendMessage(this.messageBox.getText());
        }
    }//GEN-LAST:event_messageBoxKeyPressed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
        this.requestFocus();
    }//GEN-LAST:event_formMouseClicked

    /**
     * Serverix per activar o desactivar el so
     * */
    private void soundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_soundButtonActionPerformed
        // TODO add your handling code here:
        this.makeNoise = !this.makeNoise;
        if(this.makeNoise){
            Sound.notification.play();
            this.soundButton.setText("Sound: On");
            this.showMessage("<font color='red'><b>Chat:</b> Sorolls de notificacions activats!</font>");
        }else{
            this.soundButton.setText("Sound: Off");
            this.showMessage("<font color='red'><b>Chat:</b> Sorolls de notificacions desactivats!</font>");
        }
    }//GEN-LAST:event_soundButtonActionPerformed

    /**
     * Mostra el dialeg que ens permet cambiar el color del nostre text i el codifica en rgb hexadecimal.
     * @param evt 
     */
    private void colorbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorbuttonActionPerformed
        // TODO add your handling code here:
        Color selectedColor = JColorChooser.showDialog(this, "Pick a Color", actualColor);
        color = String.format("#%02x%02x%02x", selectedColor.getRed(), selectedColor.getGreen(), selectedColor.getBlue());
        this.actualColor = selectedColor;
        this.colorbutton.setForeground(selectedColor);
    }//GEN-LAST:event_colorbuttonActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jMenu1ActionPerformed
    
    /**
     * Mostra els autors de la practica en un dialog.
     * @param evt 
     */
    private void aboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Practica 1 Xarxes \n - Cristian Muriel \n - Bruno Vescovi", "About", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_aboutActionPerformed
  

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Client client;
                client = new Client();
                client.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem about;
    private javax.swing.JScrollPane chatfield;
    private javax.swing.JButton colorbutton;
    private javax.swing.JButton connectButton;
    private javax.swing.JTextField host;
    private javax.swing.JLabel indentificador;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JTextField messageBox;
    private javax.swing.JEditorPane messagesField;
    private javax.swing.JLabel numUsers;
    private javax.swing.JTextField port;
    private javax.swing.JButton sendButton;
    private javax.swing.JButton soundButton;
    // End of variables declaration//GEN-END:variables
}
