
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *Aquesta classe conte els metodes necessaris per enviar un misatge al servidor. 
 *
 */

/**
 *
 * @author cristian
 */
public class ServerSender {
    Socket sock;
    private boolean running = true;
    Scanner sc;
    OutputStream salida;
    PrintWriter out;
    
    public ServerSender(Socket sock){
        this.sock = sock;
        this.sc =  new Scanner(System.in);
        try {
            salida = this.sock.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(ServerSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        out = new PrintWriter(salida,true);
    }

    
    public void SendMessage(String message){
        out.println(message);
    }
}
