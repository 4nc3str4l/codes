/*
 * Aquesta classe serveix per poder reproduir un so.
 * Tambe es fa en un thread a part perque sino seria bloquejant.
 */

/**
 *
 * @author cristian
 */
import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	public static final Sound notification = new Sound("test.wav");
	
	private AudioClip clip;
	
	private Sound(String name ){
		
		clip = Applet.newAudioClip(Sound.class.getResource(name));
	}
	
	public void play() {
		try {
			new Thread() {
				public void run() {
					clip.play();
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
