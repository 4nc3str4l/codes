
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Aquesta classe s'encarrega de escoltar al servidor en paralel a la GUI
 * Ja que sino la bloquejaria.
 */

/**
 *
 * @author cristian
 */
public class ServerListener implements Runnable {
    Socket sock;
    public boolean running = true;
    Client client;
    
    public ServerListener(Socket sock,Client client){
        this.sock = sock;
        this.client = client;
    }

    @Override
    public void run() {
        System.out.println("Server Listener Ready");
        InputStream entrada = null;  
        try {
            entrada = this.sock.getInputStream();
        } catch (IOException ex) {
            Logger.getLogger(ServerListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        while(running){
            Scanner in = new Scanner(entrada); 
            while(in.hasNextLine()){ 
                String linia_in = in.nextLine();
                this.client.showMessage(linia_in);
            }  
        }
    }
}
