import java.net.Socket;

/**
 * Aquesta clase representa un client com un socket i una id.
 * @author cristian
 *
 */
public class Client {
	private Socket socket;
	private int id;
	public Client(Socket socket, int id){
		this.socket = socket;
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	
	public Socket getSocket(){
		return this.socket;
	}
	
	
}
